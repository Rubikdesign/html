# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
* HTML template template to be used as a wordpress theme. (Currently there is only one page with different options -> Index.html)
* Version 1.0
* Author: Adrian Costache
### How do I get set up? ###

* git clone https://Rubikdesign@bitbucket.org/Rubikdesign/html.git
* cd html
* Open index.html in your browser, for example Chrome or Mozilla
* Open index.html
* Dependencies
 - jquery-2.2.4.min.js
 - typed.min.js
 - TweenMax.min.js
 - jquery.stellar.min.js
 - jquery.sidr.min.js
 - aos.js
 - slick.min.js
### To be considered ### 
- The code is not yet finalized, it's just the beginning part

### Next steps ###
* Create all necessary pages
* Respnsive part
* Convert to Wordpress theme for Rubiko project (https://bitbucket.org/Rubikdesign/rubiko/src/master/ )
